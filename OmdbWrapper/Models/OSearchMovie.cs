﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OmdbWrapper.Models
{
    public class OSearchMovie
    {

        public string Title { get; set; }

        public string Year { get; set; }

        public string ImdbId { get; set; }

        public string Type { get; set; }
    }
}
