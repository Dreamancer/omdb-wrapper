﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OmdbWrapper.Models;

namespace OmdbWrapper
{
    public static class Wrapper
    {
        private const string SEARCH_URI = "http://www.omdbapi.com/?s={0}{1}";
        private const string TITLE_URI = "http://www.omdbapi.com/?t={0}{1}&plot={2}";
        private const string IMDBID_URI = "http://www.omdbapi.com/?i={0}&plot={1}";

        public static T GetResponse<T>(string uri)
        {
            using (WebClient client = new WebClient())
            {
                var jsonResponse = client.DownloadString(uri);
                return JsonConvert.DeserializeObject<T>(jsonResponse);
            }
        }

        public static async Task<T> GetResponseAsync<T>(string uri)
        {
            using (WebClient client = new WebClient())
            {
                var jsonResponseTask = client.DownloadStringTaskAsync(uri);
                string jsonResponse = await jsonResponseTask;
                return JsonConvert.DeserializeObject<T>(jsonResponse);
            }
        }

        public static OMovie GetByTitle(string title)
        {
            return !string.IsNullOrEmpty(title)
                ? GetResponse<OMovie>(string.Format(TITLE_URI, WebUtility.UrlEncode(title), null, "full"))
                : null;
        }

        public static OMovie GetByTitle(string title, string year)
        {
            int i;
            return (!string.IsNullOrEmpty(title) && Int32.TryParse(year,out i))
                ? GetResponse<OMovie>(string.Format(TITLE_URI, WebUtility.UrlEncode(title),"&y=" + year, "full"))
                : null;
        }

        public static OMovie GetById(string id)
        {
            return !string.IsNullOrEmpty(id)
                ? GetResponse<OMovie>(string.Format(IMDBID_URI,id, "full"))
                : null;
        }

        public static async Task<OMovie> GetByIdAsync(string id)
        {
            if (!string.IsNullOrEmpty(id)) {
                var responseTask = GetResponseAsync<OMovie>(string.Format(IMDBID_URI, id, "full"));
                OMovie movie = await responseTask;
                return movie;
                    }
            return null;
        }

        public static IEnumerable<OSearchMovie> Search(string title, string year)
        {
            int i;
            return (!string.IsNullOrEmpty(title) && Int32.TryParse(year, out i))
                ? GetResponse<OSearchList>(string.Format(SEARCH_URI, WebUtility.UrlEncode(title), "&y=" + year)).Search
                : null;

        }

        public static IEnumerable<OSearchMovie> Search(string title)
        {
            return !string.IsNullOrEmpty(title)
                ? GetResponse<OSearchList>(string.Format(SEARCH_URI, WebUtility.UrlEncode(title), null)).Search
                : null;

        }

        public static async Task<IEnumerable<OSearchMovie>> SearchAsync(string title)
        {
           if (!string.IsNullOrEmpty(title)) {
                var responseTask = GetResponseAsync<OSearchList>(string.Format(SEARCH_URI, WebUtility.UrlEncode(title), null));
                var searchMovies = await responseTask;
                return searchMovies.Search;
                    }
                return null;
        }


    }
}
